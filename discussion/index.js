// Basic Functions

// 
/*
	The statements and instructions inside a function is not immediately executed when the function is defined.
	They are run/executed when a function is invoked.
	To invoke a declared function, add the name of the function and a parenthesis.

	*/

	function printName() {
		console.log('My Name is John');
		console.log('My Name is John');
		console.log('My Name is John');
	}
	printName();

// Function Expression
// not for hosting

let variableFunction = function myGreetings() {
	console.log('Hello');
}
variableFunction();

// Re-Assigning functions

variableFunction = function myGreetings() {
	console.log('Updated Hello');
}
variableFunction();

// Function Scoping

// Scope is the accessibility(visibility) of variables

// 1. local/block scope
// 2. global scope
// 3. function scope

//Local Variable

{
	let  localVar = "Armando Perez";
	console.log(localVar);
}

// Global Variable
let globalVar = "Mr. Worldwide";
console.log(globalVar);

// Function Scope

function showNames() {
	// Function scoped variables:
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionConst);
	console.log(functionLet);
}
showNames();

// console.log(functionConst);
// console.log(functionLet);

// alert() and prompt()

// alert() - allows us to show a small window at the top of our browser page to show information to our users.

// It, much like alert(), will have the page wait until the user completes or enters their input.

// alert('Hello, User');

// function showSampleAlert() {
// 	alert('Hello User!');
// }
// showSampleAlert();


// ===================================================================================

// prompt() - Allows us to show a small window at the top of the browser to gather user input.

let samplePrompt = prompt('Enter your Name: ');

console.log('Hello, ' + samplePrompt);

function printWelcomeMessages() {
	let firstName = prompt('Enter Your First Name');
	let lastName = prompt('Enter Your Last Name');

	console.log('Hello, ' + firstName + ' ' + lastName + '!');
	console.log('Welcome to Mobile Legends');
}

printWelcomeMessages();